//
//  PromptCredentialsOperations.swift
//  WDFoundation
//
//  Created by Jorge Galrito on 15/04/2018.
//  Copyright © 2018 WATERDOG. All rights reserved.
//

import Foundation
import WDOperations
import os.log

public struct UserCredentials {
  public let username: String
  public let password: String
  
  public init(username: String, password: String) {
    self.username = username
    self.password = password
  }
}

/**
 *  Abstract class to show input for the user to to insert its credentials.
 *
 *  This subclass is expected to call the handler and to close the UI and
 *  finish the operation if the result is `success`, so the other
 *  authentication operations can continue.
 */
open class PromptCredentialsOperation: WDOperation {
  public enum Result {
    case success
    case error(AuthenticationError)
  }

  internal(set) public var handler: ((UserCredentials, @escaping ((Result) -> Void)) -> Void)?
  
  open override func execute() {
    if #available(iOS 10, *) {
      os_log("Executing prompt credentials to user", log: authenticationLog, type: .info)
    }
  }
}
